#include "rubrica.h"

#include <QStandardItemModel>
#include <QTreeView>
#include <QStyledItemDelegate>
#include <QItemEditorFactory>
#include <QComboBox>
#include <QFileDialog>
#include <QToolBar>
#include <QFile>
#include <QDebug>
#include <QDate>
#include <QProcess>
#include <QStatusBar>
#include <QTextDocument>
#include <QTextCursor>
#include <QTextTable>
#include <QTextList>
#include <QTextDocumentWriter>
#include <QLineEdit>
#include <QMessageBox>
#include <QApplication>

#include <qjson/parser.h>

class PossibilitiesCombo : public QStyledItemDelegate
{
    Q_OBJECT
    public:
        virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& , const QModelIndex& index) const {
            QComboBox* selector = new QComboBox(parent);
            connect(selector, SIGNAL(currentIndexChanged(int)), SLOT(comboChanged()));
            QList<QVariant> vals = index.data(Rubrica::Possibilities).toList();
            selector->addItem(QString());
            foreach(const QVariant& val, vals) {
                QVariantMap entry = val.toMap();
                QVariant points = entry["points"];
                selector->addItem(tr("%1. %2").arg(points.toString()).arg(entry["description"].toString()), points);
            }
            QVariant idx = index.data(Qt::EditRole);
            if(idx.canConvert(QVariant::Int))
                selector->setCurrentIndex(idx.toInt());
            return selector;
        }

        virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
            QComboBox* selector = qobject_cast<QComboBox*>(editor);
            model->setData(index, selector->itemData(selector->currentIndex()), Rubrica::Result);
        }

        virtual void setEditorData(QWidget* editor, const QModelIndex& index) const {
            QComboBox* selector = qobject_cast<QComboBox*>(editor);
            int foundIdx = selector->findData(index.data(Rubrica::Result));
            if(foundIdx>=0)
                selector->setCurrentIndex(foundIdx);
        }

        virtual QSize sizeHint(const QStyleOptionViewItem& , const QModelIndex& ) const
        {
            return QSize(24, 24);
        }

    private slots:
        void comboChanged() {
            QWidget* combo = qobject_cast<QWidget*>(sender());
            commitData(combo);
        }
};

Rubrica::Rubrica()
    : QMainWindow()
    , m_maximum(0)
{
    m_studentName = new QLineEdit(this);
    m_studentName->setPlaceholderText(tr("Student's name..."));

    QToolBar* toolbar = new QToolBar(tr("Standard"));
    toolbar->addAction(QIcon::fromTheme("document-new"), tr("New"), this, SLOT(newRubric()));
    toolbar->addAction(QIcon::fromTheme("document-open"), tr("Open..."), this, SLOT(open()));
    toolbar->addAction(QIcon::fromTheme("document-export"), tr("Export..."), this, SLOT(exportRubric()));
    toolbar->addWidget(m_studentName);
    addToolBar(toolbar);

    m_rubric = new QStandardItemModel(this);
    m_rubric->setHorizontalHeaderLabels(QStringList() << tr("Description") << tr("Points"));
    connect(m_rubric, SIGNAL(dataChanged(QModelIndex,QModelIndex)), SLOT(countChanged()));

    m_view = new QTreeView(this);
    m_view->setRootIsDecorated(false);
    m_view->setItemDelegate(new PossibilitiesCombo);
    m_view->setModel(m_rubric);
    setCentralWidget(m_view);

    setStatusBar(new QStatusBar(this));
    countChanged();
}

Rubrica::~Rubrica()
{}

void Rubrica::newRubric()
{
    QStringList args;
    if(!m_path.isEmpty())
        args << m_path;
    QProcess::startDetached(QApplication::applicationFilePath(), args);
}

void Rubrica::open()
{
    QString url = QFileDialog::getOpenFileName(this, tr("Open a rubric"), QString(), "Rubric (*.json)");
    if(!url.isEmpty()) {
        open(url);
    }
}

void Rubrica::open(const QString& path)
{
    m_path = path;
    m_maximum = 0;
    m_rubric->clear();
    m_openedDocument.clear();
    QJson::Parser p;
    QFile f(path);
    bool ok = f.open(QIODevice::ReadOnly);
    if(!ok)
        qDebug() << "error when opening" << path;
    Q_ASSERT(ok);
    QVariant parsed = p.parse(&f, &ok);
    if(!ok)
        qDebug() << "error:" << p.errorString();
    Q_ASSERT(ok);
    m_openedDocument = parsed.toMap();
    QStandardItem* root = m_rubric->invisibleRootItem();
    QVariantList categories = m_openedDocument["data"].toList();
    foreach(const QVariant& category, categories) {
        const QMap<QString, QVariant> cat = category.toMap();
        QStandardItem* it = new QStandardItem(cat["category"].toString());
        it->setEditable(false);
        QStandardItem* itPossibilitat = new QStandardItem;
        itPossibilitat->setData(cat["possibilities"], Possibilities);
        root->appendRow(QList<QStandardItem*>() << it << itPossibilitat);

        QVariantList possibilities = cat["possibilities"].toList();
        
        double max = 0;
        foreach(const QVariant& possib, possibilities) {
            QVariantMap poss = possib.toMap();
            max = qMax(max, poss["points"].toReal());
        }
        m_maximum += max;
    }
    
    for(int i=0; i<root->rowCount(); ++i) {
        m_view->openPersistentEditor(root->child(i,1)->index());
    }
    countChanged();
}

void Rubrica::countChanged()
{
    if(m_rubric->rowCount() == 0)
        statusBar()->showMessage(tr("Open some rubric!"));
    else
        statusBar()->showMessage(tr("%1 points over %2. %3 aspects to answer").arg(countPoints()).arg(m_maximum).arg(countUnanswered()));
}

double Rubrica::countPoints() const
{
    double ret = 0;
    for(int i=0, rows=m_rubric->rowCount(); i<rows; ++i) {
        ret += m_rubric->index(i, 1).data(Result).toReal();
    }
    return ret;
}

int Rubrica::countUnanswered() const
{
    int ret = 0;
    for(int i=0, rows=m_rubric->rowCount(); i<rows; ++i) {
        ret += m_rubric->index(i, 1).data(Result).isNull();
    }
    return ret;
}

void Rubrica::exportRubric()
{
    if(countUnanswered()>0) {
        int ret = QMessageBox::question(this, tr("Unanswered questions"),
                                              tr("There are %1 unanswered questions. Are you sure you want to export?").arg(countUnanswered()),
                                              QMessageBox::Yes|QMessageBox::No, QMessageBox::No);
        if(ret != QMessageBox::Yes)
            return;
    }
    if(m_studentName->text().isEmpty()) {
        int ret = QMessageBox::question(this, tr("No name"),
                                        tr("You did not introduce the name of the student. Are you sure you want to export?"),
                                        QMessageBox::Yes|QMessageBox::No, QMessageBox::No);
        if(ret != QMessageBox::Yes)
            return;
    }
    QString path = QFileDialog::getSaveFileName(this, tr("Export rubric"), QDir::homePath(), tr("OpenDocument Text Files (*.odt)"));
    if(path.isEmpty())
        return;
    exportDocument(path);
}

void Rubrica::exportDocument(const QString& path)
{
    QTextDocument doc;
    QTextCursor cursor(&doc);
    QTextCharFormat tcf;
    tcf.setFont(doc.defaultFont());
    cursor.setCharFormat(tcf);

    //header
    QImage logo = QImage(m_openedDocument["logo"].toString());
    QTextTableFormat headerFormat;
    headerFormat.setWidth(QTextLength(QTextLength::PercentageLength, 100));
    QTextTable* table = cursor.insertTable(2, 3, headerFormat);
    if(!logo.isNull()) {
        cursor.insertImage(logo.scaled(150, 150, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
    cursor.movePosition(QTextCursor::NextCell);
    cursor.insertText(QDate::currentDate().toString(Qt::SystemLocaleShortDate));
    cursor.movePosition(QTextCursor::NextCell);
    cursor.insertText(QObject::tr("Mark: %1/%2").arg(countPoints()).arg(m_maximum));
    cursor.movePosition(QTextCursor::NextCell);
    cursor.movePosition(QTextCursor::NextCell);
    cursor.insertText(QObject::tr("Name: %1").arg(m_studentName->text()));
    cursor.movePosition(QTextCursor::NextCell);
    cursor.movePosition(QTextCursor::NextBlock);
    table->mergeCells(0,0, 2,1);
    table->mergeCells(1,1, 1,2);

    //title: subject and course
    QTextBlockFormat tbf;
    tbf.setBottomMargin(10);
    tbf.setAlignment(Qt::AlignCenter);
    cursor.insertBlock(tbf);
    QTextCharFormat titleFormat = cursor.blockCharFormat();
    titleFormat.setFontPointSize(15);
    titleFormat.setFontWeight(400);
    cursor.insertText(m_openedDocument["subject"].toString(), titleFormat);
    cursor.insertText("\n");
    QTextCharFormat subtitleFormat = cursor.blockCharFormat();
    subtitleFormat.setFontPointSize(12);
    cursor.insertText(m_openedDocument["course"].toString(), subtitleFormat);
    cursor.movePosition(QTextCursor::NextBlock);

    //the actual rubric
    QTextTableFormat ttf;
    ttf.setWidth(QTextLength(QTextLength::PercentageLength, 100));
    ttf.setBorder(10);
    ttf.setBackground(Qt::green);
    ttf.setBorderStyle(QTextFrameFormat::BorderStyle_Dotted);
    ttf.setAlignment(Qt::AlignRight);
    ttf.setColumnWidthConstraints(QVector<QTextLength>() << QTextLength(QTextLength::PercentageLength, 20)
                                                         << QTextLength(QTextLength::PercentageLength, 80));

    QTextCharFormat tableHeaderFormat = subtitleFormat;
    tableHeaderFormat.setFontWeight(400);
    cursor.insertTable(m_rubric->rowCount(), 2, ttf);
    for(int i=0, rows=m_rubric->rowCount(); i<rows; ++i) {
        QModelIndex indexAspect = m_rubric->index(i, 0), indexPossibilities = m_rubric->index(i, 1);
        cursor.insertHtml(indexAspect.data(Qt::DisplayRole).toString());
        cursor.movePosition(QTextCursor::NextCell);

        QVariantList list = indexPossibilities.data(Possibilities).toList();
        QVariant result = indexPossibilities.data(Result);
        foreach(const QVariant& possibility, list) {
            const QVariantMap map = possibility.toMap();
            if(map["points"] == result) {
                cursor.insertText("*");
            } else {
                cursor.insertText("-");
            }
            cursor.insertText(tr(" %1 point: %2").arg(map["points"].toReal()).arg(map["description"].toString()));
            cursor.insertHtml("<br />\n");
        }
        cursor.movePosition(QTextCursor::NextCell);
    }
    cursor.movePosition(QTextCursor::NextBlock);

    QTextDocumentWriter tdw(path, path.mid(path.lastIndexOf('.')+1).toLatin1().data());
    bool b = tdw.write(&doc);
    if(!b)
        qDebug() << "couldn't write!" << path << path.mid(path.lastIndexOf('.')+1);
}


#include "rubrica.moc"
#include "moc_rubrica.cpp"
