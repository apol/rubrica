#include <QtGui/QApplication>
#include "rubrica.h"


int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    Rubrica foo;
    QStringList args = app.arguments();
    args.removeFirst();
    if(!args.isEmpty()) {
        foo.open(args.takeFirst());
    }

    if(!args.isEmpty()) {
        foo.exportDocument(args.takeFirst());
        return 0;
    }

    foo.show();
    return app.exec();
}
