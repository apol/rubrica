#ifndef RUBRICA_H
#define RUBRICA_H

#include <QtGui/QMainWindow>
#include <QTreeView>

class QStandardItemModel;

class Rubrica : public QMainWindow
{
Q_OBJECT
public:
    Rubrica();
    virtual ~Rubrica();

    double countPoints() const;
    int countUnanswered() const;
    void open(const QString& path);
    void exportDocument(const QString& path);

    enum Roles {
        Possibilities = Qt::UserRole+1,
        Result
    };

private slots:
    void open();
    void exportRubric();
    void countChanged();
    void newRubric();

private:
    QStandardItemModel* m_rubric;
    QTreeView* m_view;
    double m_maximum;
    QVariantMap m_openedDocument;
    QLineEdit* m_studentName;
    QString m_path;
};

#endif // rubrica_H

