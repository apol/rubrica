#ifndef RUBRICAEDITOR_H
#define RUBRICAEDITOR_H

#include <QMainWindow>

namespace Ui {
    class DetailsForm;
}

class RTabWidget;
class QModelIndex;
class QTreeView;
class QStandardItemModel;

class RubricaEditor : public QMainWindow
{
    Q_OBJECT
    public:
        explicit RubricaEditor(QWidget* parent = 0);
        ~RubricaEditor();

        void addEntry(QStandardItemModel* model, const QString& description, qreal points);
        void openRubric(const QString& path);

    private slots:
        QStandardItemModel* addCategory(const QString& name = QString());
        void titleChanged(const QString& title, int idx = -1);
        void save();
        void open();
        void clear();
        void restartView();
        void addEntryToView();
        void removeEntryFromView();
        void cloneCurrentAspect();
        void removeCurrentAspect();
        void contentsChanged(const QModelIndex& idxA, const QModelIndex& idxB);
        void contentsRemoved();
        void logoSelected();
        void tabMoved(int from, int to);

    private:
        void setLogoPath(const QString& path);

        RTabWidget* m_tabs;
        QList<QStandardItemModel*> m_models;
        QList<QTreeView*> m_views;
        Ui::DetailsForm* m_details;
        QString m_logoPath;
};

#endif // RUBRICAEDITOR_H
