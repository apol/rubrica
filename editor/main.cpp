#include <QApplication>
#include "rubricaeditor.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    app.setApplicationName("Rubrica Editor");
    app.setApplicationVersion("0.90");

    RubricaEditor editor;
    if(app.argc()>1) {
        editor.openRubric(app.arguments().last());
    }
    editor.show();

    return app.exec();
}
