#include "rubricaeditor.h"

#include <QFormLayout>
#include <QLineEdit>
#include <QTextEdit>
#include <QDockWidget>
#include <QTabWidget>
#include <QPushButton>
#include <QTreeView>
#include <QToolBar>
#include <QStringListModel>
#include <QFileDialog>
#include <QStandardItemModel>
#include <QToolButton>
#include <QMenu>
#include <QDebug>
#include <QSettings>

#include <qjson/serializer.h>
#include <qjson/parser.h>

#include "ui_details.h"

static QVariant findMax(QAbstractItemModel* model)
{
    QVariant max;
    for(int i=0, count=model->rowCount(); i<count; ++i) {
        QModelIndex idx = model->index(i, 1);
        QVariant data = idx.data(Qt::EditRole);
        if(max.isNull() || (!data.isNull() && data.toDouble()>max.toDouble())) {
            max = data;
        }
    }
    return max;
}

//TODO: remove for Qt5, only needed to access ::tabBar()
class RTabWidget : public QTabWidget {
public:
    RTabWidget(QWidget* parent) : QTabWidget(parent) {}
    QTabBar* tabBar() const { return QTabWidget::tabBar(); }
};

RubricaEditor::RubricaEditor(QWidget* parent)
    : QMainWindow(parent)
    , m_details(new Ui::DetailsForm)
{
    QDockWidget* descriptionDock = new QDockWidget;
    QWidget* descriptionWidget = new QWidget(descriptionDock);
    m_details->setupUi(descriptionWidget);
    descriptionDock->setWidget(descriptionWidget);
    connect(m_details->findLogo, SIGNAL(clicked(bool)), this, SLOT(logoSelected()));
    addDockWidget(Qt::LeftDockWidgetArea, descriptionDock);

    QMenu* actionsMenu = new QMenu;
    actionsMenu->addAction(QIcon::fromTheme("tab-duplicate"), tr("Clone"), this, SLOT(cloneCurrentAspect()));
    actionsMenu->addAction(QIcon::fromTheme("list-remove"), tr("Remove"), this, SLOT(removeCurrentAspect()));

    m_tabs = new RTabWidget(this);
    m_tabs->setMovable(true);
    connect(m_tabs->tabBar(), SIGNAL(tabMoved(int,int)), SLOT(tabMoved(int,int)));
    QToolButton* button = new QToolButton;
    button->setText(tr("Add"));
    button->setIcon(QIcon::fromTheme("list-add"));
    button->setMenu(actionsMenu);
    button->setPopupMode(QToolButton::MenuButtonPopup);
    connect(button, SIGNAL(clicked(bool)), SLOT(addCategory()));
    m_tabs->setCornerWidget(button);
    setCentralWidget(m_tabs);

    QToolBar* toolbar = new QToolBar(this);
    toolbar->addAction(QIcon::fromTheme("document-new"), tr("New"), this, SLOT(restartView()))->setShortcut(Qt::CTRL|Qt::Key_N);
    toolbar->addAction(QIcon::fromTheme("document-save"), tr("Save"), this, SLOT(save()))->setShortcut(Qt::CTRL|Qt::Key_S);
    toolbar->addAction(QIcon::fromTheme("document-open"), tr("Open"), this, SLOT(open()))->setShortcut(Qt::CTRL|Qt::Key_O);
    addToolBar(toolbar);

    restartView();

    QSettings s;
    m_details->author->setText(s.value("author").toString());
    m_details->course->setText(s.value("course").toString());
    m_details->subject->setText(s.value("subject").toString());
    setLogoPath(s.value("logo").toString());
}

RubricaEditor::~RubricaEditor()
{
    QSettings s;
    s.setValue("author", m_details->author->text());
    s.setValue("course", m_details->course->text());
    s.setValue("subject", m_details->subject->text());
    s.setValue("logo", m_logoPath);
}

void RubricaEditor::setLogoPath(const QString& path)
{
    m_logoPath = path;
    QPixmap theLogo;
    if(!path.isEmpty()) {
        int w = m_details->author->width();
        theLogo = QPixmap(path);
        theLogo = theLogo.scaled(w,w, Qt::KeepAspectRatio);
    }
    if(theLogo.isNull()) {
        m_details->logo->setText(tr("Find image!"));
    } else
        m_details->logo->setPixmap(theLogo);
}

QStandardItemModel* RubricaEditor::addCategory(const QString& name)
{
    QWidget* w = new QWidget(m_tabs);
    QVBoxLayout* layout = new QVBoxLayout(w);
    QTreeView* view = new QTreeView(w);
    view->setRootIsDecorated(false);
    QLineEdit* line = new QLineEdit(w);
    line->setPlaceholderText(tr("Name..."));
    line->setText(name);
    line->setProperty("tab", m_tabs->count());
    view->setProperty("title", qVariantFromValue<QObject*>(line));
    connect(line, SIGNAL(textEdited(QString)), SLOT(titleChanged(QString)));
    QPushButton* addButton = new QPushButton(QIcon::fromTheme("list-add"), tr("Add"));
    connect(addButton, SIGNAL(clicked(bool)), SLOT(addEntryToView()));
    QPushButton* removeButton = new QPushButton(QIcon::fromTheme("list-remove"), tr("Remove"));
    connect(removeButton, SIGNAL(clicked(bool)), SLOT(removeEntryFromView()));
    
    layout->addWidget(line);
    layout->addWidget(view);
    layout->addWidget(addButton);
    layout->addWidget(removeButton);
    m_tabs->addTab(w, name);
    m_tabs->setCurrentIndex(m_tabs->count()-1);
    QStandardItemModel* model = new QStandardItemModel;
    model->setHorizontalHeaderLabels(QStringList(tr("Description")) << tr("Points"));
    connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), SLOT(contentsChanged(QModelIndex,QModelIndex)));
    connect(model, SIGNAL(rowsRemoved(QModelIndex,int,int)), SLOT(contentsRemoved()));
    connect(model, SIGNAL(rowsInserted(QModelIndex,int,int)), SLOT(contentsRemoved()));
    
    m_views += view;
    m_models += model;
    view->setModel(m_models.last());
    line->setFocus();
    line->selectAll();

    view->setSortingEnabled(true);
    view->sortByColumn(1, Qt::AscendingOrder);
    view->header()->setResizeMode(0, QHeaderView::Stretch);
    view->header()->setResizeMode(1, QHeaderView::ResizeToContents);
    return m_models.last();
}

void RubricaEditor::titleChanged(const QString& title, int idx)
{
    if(idx<0)
        idx = sender()->property("tab").toInt();
    QStandardItemModel* model = m_models[idx];
    QVariant max = findMax(model);

    QString newTitle;
    if(max.isNull()) {
        newTitle = title;
    } else {
        newTitle = tr("%1 (%2 points)").arg(title).arg(max.toDouble());
    }
    m_tabs->setTabText(idx, newTitle);
}

bool entryLess(const QVariant& a, const QVariant& b)
{
    Q_ASSERT(a.type()==QVariant::Map && b.type()==QVariant::Map);
    return a.toMap()["points"].toDouble() < b.toMap()["points"].toDouble();
}

QVariantList concatenateEntries(QAbstractItemModel* model)
{
    QVariantList ret;
    for(int i=0, rows=model->rowCount(); i<rows; ++i) {
        QVariantMap entry;
        entry["description"] = model->index(i, 0).data(Qt::DisplayRole);
        entry["points"] = model->index(i, 1).data(Qt::DisplayRole).toReal();
        ret += entry;
    }
    qSort(ret.begin(), ret.end(), entryLess);
    return ret;
}

void RubricaEditor::addEntryToView()
{
    int idx = m_tabs->currentIndex();
    QStandardItemModel* m = m_models[idx];
    QVariant max = findMax(m);
    qreal next = 0.;
    if(!max.isNull())
        next = max.toDouble()+1;
    addEntry(m, QString(), next);

    QModelIndex idxM = m->index(m->rowCount()-1, 0);
    m_views[idx]->edit(idxM);
    m_views[idx]->selectionModel()->select(idxM, QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
}

bool greaterThan(const QModelIndex& a, const QModelIndex& b)
{
    return a.row()>b.row();
}

void RubricaEditor::removeEntryFromView()
{
    int idx = m_tabs->currentIndex();
    QStandardItemModel* model = m_models[idx];
    QModelIndexList selected = m_views[idx]->selectionModel()->selectedRows();
    qSort(selected.begin(), selected.end(), greaterThan);
    foreach(const QModelIndex& idx, selected) {
        model->removeRow(idx.row());
    }
}

void RubricaEditor::save()
{
    QString file = QFileDialog::getSaveFileName(this, tr("Rubric to save"));
    if(!file.isEmpty()) {
        QFile f(file);
        bool ok = f.open(QIODevice::WriteOnly);
        Q_ASSERT(ok);

        QVariantList values;
        for(int i=0; i<m_models.size(); ++i) {
            QStandardItemModel* model = m_models[i];
            QTreeView* view = m_views[i];
            QLineEdit* title = qobject_cast<QLineEdit*>(view->property("title").value<QObject*>());
            QVariantMap entry;
            entry.insert("category", title->text());
            entry.insert("possibilities", concatenateEntries(model));
            values.append(entry);
        }

        QVariantMap res;
        res["author"] = m_details->author->text();
        res["comments"] = m_details->comments->toPlainText();
        res["course"] = m_details->course->text();
        res["name"] = m_details->name->text();
        res["subject"] = m_details->subject->text();
        res["logo"] = m_logoPath;
        res["data"] = values;

        QJson::Serializer s;
        s.serialize(res, &f, &ok);
        Q_ASSERT(ok);
    }
}

void RubricaEditor::open()
{
    QString file = QFileDialog::getOpenFileName(this, tr("Rubric to edit"));
    if(!file.isEmpty()) {
        openRubric(file);
    }
}

void RubricaEditor::openRubric(const QString& file)
{
    clear();
    QFile f(file);
    bool ok = f.open(QIODevice::ReadOnly);
    Q_ASSERT(ok);

    QJson::Parser p;
    QVariant parsed = p.parse(&f, &ok);
    Q_ASSERT(ok && parsed.type() == QVariant::Map);
    const QVariantMap content = parsed.toMap();
    m_details->author->setText(content["author"].toString());
    m_details->comments->setPlainText(content["comments"].toString());
    m_details->course->setText(content["course"].toString());
    m_details->name->setText(content["name"].toString());
    m_details->subject->setText(content["subject"].toString());
    setLogoPath(content["logo"].toString());

    QVariantList vals = content["data"].toList();
    foreach(const QVariant& cat, vals) {
        const QVariantMap category = cat.toMap();
        QString c = category["category"].toString();
        QStandardItemModel* model = addCategory(c);
        QVariantList possibilities = category["possibilities"].toList();
        foreach(const QVariant& poss, possibilities) {
            QVariantMap possibility = poss.toMap();
            addEntry(model, possibility["description"].toString(), possibility["points"].toDouble());
        }
        titleChanged(c, m_models.size()-1);
        m_views[m_models.indexOf(model)]->sortByColumn(1, Qt::AscendingOrder);
    }
}

void RubricaEditor::addEntry(QStandardItemModel* model, const QString& description, qreal points)
{
    QStandardItem* pointsItem = new QStandardItem(QString::number(points));
    pointsItem->setData(points, Qt::EditRole);
    model->appendRow(QList<QStandardItem*>() << new QStandardItem(description) << pointsItem);
}

void RubricaEditor::clear()
{
    m_tabs->clear();
    qDeleteAll(m_models);
    qDeleteAll(m_views);
    m_models.clear();
    m_views.clear();
}

void RubricaEditor::restartView()
{
    clear();
    addCategory();
}

void RubricaEditor::cloneCurrentAspect()
{
    int prevIdx = m_tabs->currentIndex();
    QStandardItemModel* prevModel = m_models[prevIdx];
    QStandardItemModel* model = addCategory();
    for(int i=0, rows=prevModel->rowCount(); i<rows; ++i) {
        QModelIndex idxA = prevModel->index(i, 0);
        QModelIndex idxB = prevModel->index(i, 1);

        addEntry(model, idxA.data(Qt::DisplayRole).toString(), idxB.data(Qt::EditRole).toDouble());
    }
}

void RubricaEditor::removeCurrentAspect()
{
    int idx = m_tabs->currentIndex();
    m_tabs->removeTab(idx);
    delete m_views.takeAt(idx);
    delete m_models.takeAt(idx);

    if(m_models.isEmpty())
        addCategory();
}

void RubricaEditor::contentsChanged(const QModelIndex&, const QModelIndex& idxB)
{
    int idx = m_models.indexOf(qobject_cast<QStandardItemModel*>(sender()));
    m_views[idx]->sortByColumn(1, Qt::AscendingOrder);
    if(idxB.column()==1) {
        QLineEdit* title = qobject_cast<QLineEdit*>(m_views[idx]->property("title").value<QObject*>());
        titleChanged(title->text(), idx);
    }
}

void RubricaEditor::contentsRemoved()
{
    int idx = m_models.indexOf(qobject_cast<QStandardItemModel*>(sender()));
    QLineEdit* title = qobject_cast<QLineEdit*>(m_views[idx]->property("title").value<QObject*>());
    titleChanged(title->text(), idx);
}

void RubricaEditor::logoSelected()
{
    QString file = QFileDialog::getOpenFileName(this, tr("Select a logo"), QString(), tr("Pictures (*.png *.jpg)"));
    if(!file.isEmpty()) {
        setLogoPath(file);
    }
}

void RubricaEditor::tabMoved(int from, int to)
{
    m_models.move(from, to);
    m_views.move(from, to);
}
